# VSCodeWithOpenGLConfig

#UsingVSCode_F5_button
**Install and config**
* create Folder Compiler in C
* copy mingw into folder Complier and Rename it "mingw64"
* copy folder include in 
>  freeglut into "C:/Compiler/mingw64/x86_64-w64-mingw32"
* copy folder lib or lin/x64 into "
>  C:/Compiler/mingw64/x86_64-w64-mingw32/lib"
* copy freeglut.dll in freeglut/bin into 
> System32 
and 
> SysWOW64 
* ** Clone This project**
* open 
> filename.c 
* Click F5 or Terminal and Start Debugging
* Done


#UsingBatFile
* **How to Use this bat file **
* ** Follow step by step below**
* **Install or Download MingGW and save it in a specific folder**
* **Download Freeglut and config it below**
* copy include and bin folder into MinGW
* go to "bin folder" and copy freeglut.dll in to System32 and SysWOW64
* **Extract the bat.(zip) and copy it into your folder**
* **Set your project name like this "main.c"**
* **Run This.bat **
* **Done**